module App.View

open Elmish
open Elmish.Browser.Navigation
open Elmish.Browser.UrlParser
open Fable.Core
open Fable.Core.JsInterop
open Fable.Import
open Fable.Import.Browser
open Types
open App.State
open Global

importAll "../sass/main.sass"

open Fable.Helpers.React
open Fable.Helpers.React.Props

open Navbar.View

let root model dispatch =

  let pageHtml (page:Page) =
    match page with
    | Home ->
        (div [ Style [PaddingLeft 50; PaddingRight 50; PaddingTop 10] ] (Home.home dispatch model) )

  div
    []
    [ div [ ClassName "content" ]
          [ navbar dispatch model.HamburgerActivated
            pageHtml model.CurrentPage
             ] ]

let safeParseHash (parser:Parser<Page->Page,Page>) (location: Location) =
    if String.length location.hash = 0 then
        None
    else
        parseHash parser location

open Elmish.React
open Elmish.Debug
open Elmish.HMR

// App
Program.mkProgram init update root
|> Program.toNavigable (safeParseHash pageParser) urlUpdate
#if DEBUG
|> Program.withDebugger
#endif
|> Program.withReact "elmish-app"
|> Program.run
