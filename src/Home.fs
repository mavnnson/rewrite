module Home

open App.Types
open Fable.Helpers.React
open Fable.Helpers.React.Props

let block condition dispatch =
    div [] [ label [ ClassName "checkbox" ] [ input [ Type "checkbox"
                                                      OnClick (fun _ -> dispatch (ToggleCondition condition)) ]
                                              str (sprintf " I have %s" condition) ] ]

let home dispatch model =
    [ p [] [ str "Welcome to rewrite!" ]
      block "dyslexia" dispatch
      block "blindness" dispatch
      block "ADHD" dispatch
      block "aspergers" dispatch
      block "autism" dispatch
      block "down syndrome" dispatch
      block "dispraxia" dispatch
      block "SPD (sensory processing disorder)" dispatch
      block "disgraphia" dispatch
      textarea [ ClassName "textarea"
                 Id "problems"
                 Placeholder "i.e. Trouble reading"
                 OnInput (fun e -> dispatch (UpdateProblemString e.Value)) ] [ ]
      p [] [ str "Type your problems/issues here. One issue at a time." ]
      div [ ClassName "button"
            OnClick (fun _ -> dispatch PushProblems) ]
          [ str "Submit!" ]
      div [] [ for i in model.Conditions do
                 yield if i.Confidence > 25 then p [] [ str (sprintf "We think you might have %s" i.Condition) ] else p [] [] ] ]
