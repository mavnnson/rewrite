module Navbar.View

open Fable.Helpers.React
open Fable.Helpers.React.Props
open App.Types

let brand dispatch hamburgerActivated =
    let hamburgerStatus =
        match hamburgerActivated with
        | true -> "is-active"
        | false -> ""
    div [ ClassName "navbar-brand" ] [
       a [ ClassName "navbar-item title in-navbar is-3"
           Href "/#home" ] [ str "Rewrite" ]
       a [ ClassName (sprintf "navbar-burger burger %s" hamburgerStatus)
           OnClick (fun _ -> dispatch ToggleHamburger) ] [
           span [] []
           span [] []
           span [] []
          ]
       ]

let buttons dispatch hamburgerActivated =
    let hamburgerStatus =
        match hamburgerActivated with
        | true -> "is-active"
        | false -> ""
    div [ ClassName (sprintf "navbar-menu %s" hamburgerStatus) ] [
        div [ ClassName "navbar-end" ] [
            div [ ClassName "navbar-item buttons" ] [
              a [ ClassName "button is-primary" ] [ str "Log in" ]
              a [ ClassName "button is-info" ] [ str "Sign up" ] ]
          ]
        ]

let navbar dispatch hamburgerActivated =
    div [ ClassName "navbar is-link"] [
          brand dispatch hamburgerActivated
        ]
