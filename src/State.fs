module App.State

open Elmish
open Elmish.Browser.Navigation
open Elmish.Browser.UrlParser
open Fable.PowerPack
open Fable.PowerPack.Fetch
open Fable.Import.Browser
open Thoth.Json

open Global
open Types

let pageParser: Parser<Page->Page,Page> =
    oneOf [
        map Home (s "")
    ]

let urlUpdate (result : Page option) model =
    match result with
    | None ->
        { model with CurrentPage = Home }, Navigation.modifyUrl "#home"
    | Some page ->
        { model with CurrentPage = page }, []

let init result =
    let (model, cmd) =
        urlUpdate result
          { CurrentPage = Home
            Conditions = []
            Problems = ""
            HamburgerActivated = false }

    model, cmd

let getPrediction text =
    promise {
        let url = (sprintf "https://cors-anywhere.herokuapp.com/https://machinelearningforkids.co.uk/api/scratch/4c54f7b0-105a-11eb-b202-1317cac500d186c5bc1a-75ba-4bf9-adb5-da2e62d7189f/classify?data=%s" text)
        let decoder = Decode.list Condition.Decoder
        let props =
            [ RequestProperties.Method HttpMethod.GET
              requestHeaders [ HttpRequestHeaders.Accept "application/json"; Origin "" ]
              ]
        let! predictionList =
          fetchAs url decoder props
        let prediction = (List.head predictionList)
        return prediction
    }

let update msg model =
    match msg with
    | DisactivateHamburger ->
        { model with HamburgerActivated = false }, Cmd.none
    | Error excep ->
        model, Cmd.none
    | ToggleHamburger ->
        { model with HamburgerActivated = not model.HamburgerActivated }, Cmd.none
    | AddCondition condition ->
        { model with Conditions = (List.append model.Conditions [ condition ]) }, Cmd.none
    | UpdateProblemString string ->
        { model with Problems = string }, Cmd.none
    | ToggleCondition string ->
        let condition = { Condition = string; Confidence = 100 }
        if not (List.contains condition model.Conditions) then
          { model with Conditions = (List.append model.Conditions [ condition ]) }, Cmd.none
        else
          { model with Conditions = (model.Conditions |> List.toArray |> Array.filter ((<>) condition ) |> Array.toList) }, Cmd.none
    | PushProblems ->
        model, Cmd.ofPromise getPrediction model.Problems AddCondition Error
