module App.Types
open Thoth.Json

open Global

type Condition =
    { Condition: string
      Confidence: int }
    static member Decoder : Decode.Decoder<Condition> =
        Decode.map2 (fun x y ->
                { Condition = x
                  Confidence = y } : Condition)
            (Decode.field "class_name" Decode.string)
            (Decode.field "confidence" Decode.int)

type Msg =
    | ToggleHamburger
    | Error of System.Exception
    | AddCondition of Condition
    | UpdateProblemString of string
    | ToggleCondition of string
    | DisactivateHamburger
    | PushProblems

type Model =
    { CurrentPage: Page
      HamburgerActivated: bool
      Conditions: List<Condition>
      Problems: string
      }
